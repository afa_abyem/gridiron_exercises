# gridiron_exercises


## Name
This is a practice exercise

## Description
This is a GitLab CI/CD pipeline definition file that defines three stages: build, test, and deploy. The pipeline has two jobs, list_files and run_ansible, which are executed in the build and test stages, respectively. Here's what each job does:

list_files job: This job is executed in the build stage and uses the alpine:latest Docker image. It runs the command ls > output.json to list the files in the current directory and stores the output in a file called output.json. The job also defines an artifact that includes the output.json file.

run_ansible job: This job is executed in the test stage and uses the ansible/ansible:latest Docker image. It runs the ansible-playbook command with the get-files.yml playbook and the newservers inventory file, and the vars.yml file containing variables. This job executes the playbook to get the files from the remote servers specified in the newservers inventory file.

